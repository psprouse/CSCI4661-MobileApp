package com.freelancefindr;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.TextPaint;
import android.text.style.URLSpan;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class Utilities
{

    public static final String EMAIL = "com.freelancefindr.EMAIL";

    /*
    public static final String NAME = "com.freelancefindr.NAME";
    public static final String JOB_TITLE = "com.freelancefindr.JOB_TITLE";
    public static final String COMPANY = "com.freelancefindr.COMPANY";
    public static final String WEBSITE = "com.freelancefindr.WEBSITE";
    public static final String LOCATION = "com.freelancefindr.LOCATION";
    public static final String PHONE = "com.freelancefindr.PHONE";
    public static final String PASSWORD = "com.freelancefindr.PASSWORD";
    public static final String ACCOUNT_TYPE = "com.freelancefindr.ACCOUNT_TYPE";
    */

    public enum MenuState
    {
        FREELANCER_LOGGED_IN,
        CLIENT_LOGGED_IN,
        LOGGED_OUT
    }

    public static MenuState menuState = MenuState.LOGGED_OUT;

    public static final ArrayList<Account> accounts = new ArrayList<>();

    private static boolean initialized = false;

    public static void Init()
    {
        if (!initialized)
        {
            initialized = true;

            ClientAccount ca = new ClientAccount("mike@orlandosteel.com", "password");
            ca.setPhoneNumber("(321) 885-9982");
            ca.setWebsite("http://orlandosteel.com");
            ca.setName("Michael Scott");
            ca.setLocation("Orlando, FL");
            ca.setCompany("Orlando Steel");

            FreelancerAccount fa = new FreelancerAccount("me@jonnguyen.com", "password");
            fa.setPhoneNumber("(323) 752-0086");
            fa.setWebsite("http://jonnguyen.com");
            fa.setName("Jon Nguyen");
            fa.setLocation("Los Angeles, CA");
            fa.setTitle("Web Developer");
            fa.setBio("My name is Jon and I am a Web Developer based in LA. I am proficient in CSS, HTML, Javascript, and Python.");

            FreelancerAccount fa2 = new FreelancerAccount("stacyj123@gmail.com", "password");
            fa2.setPhoneNumber("(756) 394-5834");
            fa2.setWebsite("http://stacysstories.tumblr.com");
            fa2.setName("Stacy Johnson");
            fa2.setLocation("Austin, TX");
            fa2.setTitle("Writer");
            fa2.setBio("Hi I'm Stacy and I am a writer in Austin, TX. I prefer to write short stories and poetry, however I am not afraid of writing longer pieces.");
            //fa2.setImage(ContextCompat.getDrawable(c, R.drawable.stacy));

            FreelancerAccount fa3 = new FreelancerAccount("rbabin@babindesign.com", "password");
            fa3.setPhoneNumber("(281) 330-8004");
            fa3.setWebsite("http://babindesign.com");
            fa3.setName("Randy Babin");
            fa3.setLocation("St. Louis, MO");
            fa3.setTitle("Graphic Designer");
            fa3.setBio("Randy here and I'm a graphic designer. I typically like working with logos but can also be commissioned for other, more creative pieces.");
            //fa3.setImage(ContextCompat.getDrawable(c, R.drawable.randy));

            accounts.add(ca);
            accounts.add(fa);
            accounts.add(fa2);
            accounts.add(fa3);
        }
    }

    public static void ShowToast(Context c, String s, boolean centered)
    {
        Toast toast = Toast.makeText(c, s, Toast.LENGTH_LONG);
        if (centered)
        {
            TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
            if (v != null)
                v.setGravity(Gravity.CENTER);
        }
        toast.show();
    }

    public static void removeUnderlines(Spannable p_Text)
    {
        URLSpan[] spans = p_Text.getSpans(0, p_Text.length(), URLSpan.class);

        for (URLSpan span : spans)
        {
            int start = p_Text.getSpanStart(span);
            int end = p_Text.getSpanEnd(span);
            p_Text.removeSpan(span);
            span = new URLSpanNoUnderline(span.getURL());
            p_Text.setSpan(span, start, end, 0);
        }
    }


    @SuppressLint("ParcelCreator")
    private static class URLSpanNoUnderline extends URLSpan {
        public URLSpanNoUnderline(String p_Url) {
            super(p_Url);
        }
        public void updateDrawState(TextPaint p_DrawState) {
            super.updateDrawState(p_DrawState);
            p_DrawState.setUnderlineText(false);
        }
    }

}