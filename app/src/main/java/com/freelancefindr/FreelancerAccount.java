package com.freelancefindr;

public class FreelancerAccount extends Account {

    private String bio, title;

    public FreelancerAccount(String e, String p) {
        super(e, p, AccountType.FREELANCER);
    }

    public void setBio(String b) {
        bio = b;
    }

    public void setTitle(String t) {
        title = t;
    }

    public String getBio() {
        return bio;
    }

    public String getTitle() {
        return title;
    }

}
