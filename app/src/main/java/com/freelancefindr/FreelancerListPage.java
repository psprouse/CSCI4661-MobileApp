package com.freelancefindr;

import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class FreelancerListPage extends AppCompatActivity
{

    RelativeLayout rootLayout;
    ArrayList<RelativeLayout> innerLayouts;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_freelancer_list_page);

        rootLayout = (RelativeLayout) findViewById(R.id.activity_freelancer_list_page);
        innerLayouts = new ArrayList<>();

        int counter = 0;
        for (Account a : Utilities.accounts)
        {
            if (a.getAccountType() == Account.AccountType.FREELANCER)
            {
                CreateNewListElement(counter, (FreelancerAccount)a);
                counter++;
            }
        }

        for (RelativeLayout r : innerLayouts)
        {
            rootLayout.addView(r);
        }
    }

    // This is a long and scary method, but it'll be cleaned up when I have more time
    private void CreateNewListElement(int c, FreelancerAccount account)
    {

        // Create the base layout for our element
        RelativeLayout newElement = new RelativeLayout(this);
        newElement.setId(View.generateViewId());

        // Parameters for the base layout
        RelativeLayout.LayoutParams parentParam = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT
        );
        if (c > 0)
        {
            parentParam.addRule(RelativeLayout.BELOW, innerLayouts.get(c - 1).getId());
        }

        // Allowing us to use dp for margins by converting it to px
        int margin = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                50,
                getResources().getDisplayMetrics()
        );
        parentParam.bottomMargin = margin;

        newElement.setLayoutParams(parentParam);

        // Parameters for the image element
        RelativeLayout.LayoutParams imageParam = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT
        );
        margin = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                30,
                getResources().getDisplayMetrics()
        );
        imageParam.setMarginEnd(margin);
        imageParam.width = 200;
        imageParam.height = 200;

        // Create our image
        ImageView image = new ImageView(this);
        image.setId(View.generateViewId());
        image.setLayoutParams(imageParam);
        image.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ff));
        newElement.addView(image);

        // Create our textviews
        TextView name = new TextView(this);
        name.setText(account.getName());
        name.setId(View.generateViewId());
        name.setTextColor(ContextCompat.getColor(this, R.color.defaultTextColor));

        TextView title = new TextView(this);
        title.setText(account.getTitle());
        title.setId(View.generateViewId());
        title.setTextColor(ContextCompat.getColor(this, R.color.defaultTextColor));

        TextView location = new TextView(this);
        location.setText(account.getLocation());
        location.setId(View.generateViewId());
        location.setTextColor(ContextCompat.getColor(this, R.color.defaultTextColor));

        // Parameters for name
        RelativeLayout.LayoutParams textParam = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT
        );
        textParam.addRule(RelativeLayout.END_OF, image.getId());
        name.setLayoutParams(textParam);

        // Parameters for title
        textParam = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT
        );
        textParam.addRule(RelativeLayout.END_OF, image.getId());
        textParam.addRule(RelativeLayout.BELOW, name.getId());
        title.setLayoutParams(textParam);

        // Parameters for location
        textParam = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT
        );
        textParam.addRule(RelativeLayout.END_OF, image.getId());
        textParam.addRule(RelativeLayout.BELOW, title.getId());
        location.setLayoutParams(textParam);

        newElement.addView(name);
        newElement.addView(title);
        newElement.addView(location);

        final FreelancerAccount finalAccount = account;
        newElement.setClickable(true);
        newElement.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent i = new Intent(FreelancerListPage.this, FreelancerProfilePage.class);
                i.putExtra(Utilities.EMAIL, finalAccount.getEmail());
                FreelancerListPage.this.startActivity(i);
            }
        });

        innerLayouts.add(newElement);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        if (Account.isLoggedIn && Account.loggedInAccount.getAccountType() == Account.AccountType.CLIENT)
        {
            getMenuInflater().inflate(R.menu.client_logged_in_menu, menu);
            Utilities.menuState = Utilities.MenuState.CLIENT_LOGGED_IN;
        }
        else if (Account.isLoggedIn && Account.loggedInAccount.getAccountType() == Account.AccountType.FREELANCER)
        {
            getMenuInflater().inflate(R.menu.freelancer_logged_in_menu, menu);
            Utilities.menuState = Utilities.MenuState.FREELANCER_LOGGED_IN;
        }
        else
        {
            getMenuInflater().inflate(R.menu.logged_out_menu, menu);
            Utilities.menuState = Utilities.MenuState.LOGGED_OUT;
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        if (Utilities.menuState == Utilities.MenuState.LOGGED_OUT)
        {
            if (id == R.id.menuItemLogin)
            {
                Intent i = new Intent(this, LoginPage.class);
                startActivity(i);
                return true;
            }
            else if (id == R.id.menuItemRegister)
            {
                Intent i = new Intent(this, MainActivity.class);
                startActivity(i);
                return true;
            }
            else if (id == R.id.menuItemFreelancers)
            {
                Intent i = new Intent(this, FreelancerListPage.class);
                startActivity(i);
                return true;
            }
            else if (id == R.id.menuItemQuit)
            {
                finishAffinity();
                return true;
            }
        }

        else if (Utilities.menuState == Utilities.MenuState.CLIENT_LOGGED_IN)
        {
            if (id == R.id.menuItemProfile)
            {
                Intent i = new Intent(this, ClientProfilePage.class);
                startActivity(i);
                return true;
            }
            else if (id == R.id.menuItemFreelancers)
            {
                Intent i = new Intent(this, FreelancerListPage.class);
                startActivity(i);
                return true;
            }
            else if (id == R.id.menuItemLogout)
            {
                Account.isLoggedIn = false;
                Account.loggedInAccount = null;
                startActivity(new Intent(this, MainActivity.class));
                return true;
            }
            else if (id == R.id.menuItemQuit)
            {
                finishAffinity();
                return true;
            }
        }

        else if (Utilities.menuState == Utilities.MenuState.FREELANCER_LOGGED_IN)
        {
            if (id == R.id.menuItemProfile)
            {
                Intent i = new Intent(this, FreelancerProfilePage.class);
                startActivity(i);
                return true;
            }
            else if (id == R.id.menuItemLogout)
            {
                Account.isLoggedIn = false;
                Account.loggedInAccount = null;
                startActivity(new Intent(this, MainActivity.class));
                return true;
            }
            else if (id == R.id.menuItemQuit)
            {
                finishAffinity();
                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }
}