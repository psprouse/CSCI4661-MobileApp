package com.freelancefindr;

import android.content.Intent;
import android.graphics.Paint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class LoginPage extends AppCompatActivity {

    private EditText email, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_page);

        /*
        if (Account.isLoggedIn) {
            if (Account.loggedInAccount.getAccountType() == Account.AccountType.CLIENT)
                setIntent(new Intent(this, ClientProfilePage.class));
            else
                setIntent(new Intent(this, FreelancerProfilePage.class));
        }
        */

        email = (EditText) findViewById(R.id.loginPageEmailField);
        password = (EditText) findViewById(R.id.loginPagePasswordField);

        Button button = (Button) findViewById(R.id.loginPageRegisterLink);
        try {
            button.setPaintFlags(button.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        } catch (Exception e) {
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        if (Account.isLoggedIn && Account.loggedInAccount.getAccountType() == Account.AccountType.CLIENT)
        {
            getMenuInflater().inflate(R.menu.client_logged_in_menu, menu);
            Utilities.menuState = Utilities.MenuState.CLIENT_LOGGED_IN;
        }
        else if (Account.isLoggedIn && Account.loggedInAccount.getAccountType() == Account.AccountType.FREELANCER)
        {
            getMenuInflater().inflate(R.menu.freelancer_logged_in_menu, menu);
            Utilities.menuState = Utilities.MenuState.FREELANCER_LOGGED_IN;
        }
        else
        {
            getMenuInflater().inflate(R.menu.logged_out_menu, menu);
            Utilities.menuState = Utilities.MenuState.LOGGED_OUT;
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        if (Utilities.menuState == Utilities.MenuState.LOGGED_OUT)
        {
            if (id == R.id.menuItemLogin)
            {
                Intent i = new Intent(this, LoginPage.class);
                startActivity(i);
                return true;
            }
            else if (id == R.id.menuItemRegister)
            {
                Intent i = new Intent(this, MainActivity.class);
                startActivity(i);
                return true;
            }
            else if (id == R.id.menuItemFreelancers)
            {
                Intent i = new Intent(this, FreelancerListPage.class);
                startActivity(i);
                return true;
            }
            else if (id == R.id.menuItemQuit)
            {
                finishAffinity();
                return true;
            }
        }

        else if (Utilities.menuState == Utilities.MenuState.CLIENT_LOGGED_IN)
        {
            if (id == R.id.menuItemProfile)
            {
                Intent i = new Intent(this, ClientProfilePage.class);
                startActivity(i);
                return true;
            }
            else if (id == R.id.menuItemFreelancers)
            {
                Intent i = new Intent(this, FreelancerListPage.class);
                startActivity(i);
                return true;
            }
            else if (id == R.id.menuItemLogout)
            {
                Account.isLoggedIn = false;
                Account.loggedInAccount = null;
                startActivity(new Intent(this, MainActivity.class));
                return true;
            }
            else if (id == R.id.menuItemQuit)
            {
                finishAffinity();
                return true;
            }
        }

        else if (Utilities.menuState == Utilities.MenuState.FREELANCER_LOGGED_IN)
        {
            if (id == R.id.menuItemProfile)
            {
                Intent i = new Intent(this, FreelancerProfilePage.class);
                startActivity(i);
                return true;
            }
            else if (id == R.id.menuItemLogout)
            {
                Account.isLoggedIn = false;
                Account.loggedInAccount = null;
                startActivity(new Intent(this, MainActivity.class));
                return true;
            }
            else if (id == R.id.menuItemQuit)
            {
                finishAffinity();
                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    public void onClickLogin(View v) {
        String e = email.getText().toString();
        String p = password.getText().toString();

        boolean success = false;
        for (Account a : Utilities.accounts) {
            if (a.checkCredentials(e, p)) {
                success = true;

                Intent i;
                if (a.getAccountType() == Account.AccountType.CLIENT)
                    i = new Intent(this, ClientProfilePage.class);
                else
                    i = new Intent(this, FreelancerProfilePage.class);

                Account.isLoggedIn = true;
                Account.loggedInAccount = a;

                i.putExtra(Utilities.EMAIL, e);
                startActivity(i);
            }
        }

        if (!success)
            Utilities.ShowToast(this, getString(R.string.loginFailed), true);
    }

    public void onClickRegister(View v) {
        startActivity(new Intent(this, MainActivity.class));
    }

}
