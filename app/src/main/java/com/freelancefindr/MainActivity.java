package com.freelancefindr;

import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class MainActivity extends AppCompatActivity implements QuitDialog.QuitDialogListener
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Utilities.Init();
    }

    @Override
    public void onBackPressed()
    {
        new QuitDialog().show(getFragmentManager(), "quitDialog");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        if (Account.isLoggedIn && Account.loggedInAccount.getAccountType() == Account.AccountType.CLIENT)
        {
            getMenuInflater().inflate(R.menu.client_logged_in_menu, menu);
            Utilities.menuState = Utilities.MenuState.CLIENT_LOGGED_IN;
        }
        else if (Account.isLoggedIn && Account.loggedInAccount.getAccountType() == Account.AccountType.FREELANCER)
        {
            getMenuInflater().inflate(R.menu.freelancer_logged_in_menu, menu);
            Utilities.menuState = Utilities.MenuState.FREELANCER_LOGGED_IN;
        }
        else
        {
            getMenuInflater().inflate(R.menu.logged_out_menu, menu);
            Utilities.menuState = Utilities.MenuState.LOGGED_OUT;
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        if (Utilities.menuState == Utilities.MenuState.LOGGED_OUT)
        {
            if (id == R.id.menuItemLogin)
            {
                Intent i = new Intent(this, LoginPage.class);
                startActivity(i);
                return true;
            }
            else if (id == R.id.menuItemRegister)
            {
                Intent i = new Intent(this, MainActivity.class);
                startActivity(i);
                return true;
            }
            else if (id == R.id.menuItemFreelancers)
            {
                Intent i = new Intent(this, FreelancerListPage.class);
                startActivity(i);
                return true;
            }
            else if (id == R.id.menuItemQuit)
            {
                finishAffinity();
                return true;
            }
        }

        else if (Utilities.menuState == Utilities.MenuState.CLIENT_LOGGED_IN)
        {
            if (id == R.id.menuItemProfile)
            {
                Intent i = new Intent(this, ClientProfilePage.class);
                startActivity(i);
                return true;
            }
            else if (id == R.id.menuItemFreelancers)
            {
                Intent i = new Intent(this, FreelancerListPage.class);
                startActivity(i);
                return true;
            }
            else if (id == R.id.menuItemLogout)
            {
                Account.isLoggedIn = false;
                Account.loggedInAccount = null;
                startActivity(new Intent(this, MainActivity.class));
                return true;
            }
            else if (id == R.id.menuItemQuit)
            {
                finishAffinity();
                return true;
            }
        }

        else if (Utilities.menuState == Utilities.MenuState.FREELANCER_LOGGED_IN)
        {
            if (id == R.id.menuItemProfile)
            {
                Intent i = new Intent(this, FreelancerProfilePage.class);
                startActivity(i);
                return true;
            }
            else if (id == R.id.menuItemLogout)
            {
                Account.isLoggedIn = false;
                Account.loggedInAccount = null;
                startActivity(new Intent(this, MainActivity.class));
                return true;
            }
            else if (id == R.id.menuItemQuit)
            {
                finishAffinity();
                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    public void onClickLogin(View v)
    {
        Intent i = new Intent(this, LoginPage.class);
        startActivity(i);
    }

    public void onClickCreateFreelancer(View v)
    {
        Intent i = new Intent(this, RegisterFreelancerPage.class);
        startActivity(i);
    }

    public void onClickCreateClient(View v)
    {
        Intent i = new Intent(this, RegisterClientPage.class);
        startActivity(i);
    }

    public void onDialogPositiveClick(DialogFragment dialog)
    {
        finishAffinity();
    }

    public void onDialogNegativeClick(DialogFragment dialog)
    {
        dialog.dismiss();
    }

}
