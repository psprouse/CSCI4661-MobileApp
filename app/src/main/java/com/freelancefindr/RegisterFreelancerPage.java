package com.freelancefindr;

import android.content.Intent;
import android.graphics.Paint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.ClientCertRequest;
import android.widget.Button;
import android.widget.EditText;

public class RegisterFreelancerPage extends AppCompatActivity
{

    private EditText email, password, confirmPassword, phone, website, name, location, title, bio;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_freelancer_page);

        /*
        if (Account.isLoggedIn)
        {
            if (Account.loggedInAccount.getAccountType() == Account.AccountType.CLIENT)
            {
                setIntent(new Intent(this, ClientProfilePage.class));
            }
            else
            {
                setIntent(new Intent(this, FreelancerProfilePage.class));
            }
        }
        */

        Button button = (Button) findViewById(R.id.registerFreelancerLoginLink);
        Button button2 = (Button) findViewById(R.id.registerFreelancerClientRegisterLink);
        try
        {
            button.setPaintFlags(button.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            button2.setPaintFlags(button.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        }
        catch (Exception e)
        {
        }

        email = (EditText) findViewById(R.id.registerFreelancerEmailField);
        password = (EditText) findViewById(R.id.registerFreelancerPasswordField);
        confirmPassword = (EditText) findViewById(R.id.registerFreelancerConfirmPasswordField);
        phone = (EditText) findViewById(R.id.registerFreelancerPhoneField);
        website = (EditText) findViewById(R.id.registerFreelancerWebsiteField);
        name = (EditText) findViewById(R.id.registerFreelancerNameField);
        location = (EditText) findViewById(R.id.registerFreelancerLocationField);
        title = (EditText) findViewById(R.id.registerFreelancerJobTitleField);
        bio = (EditText) findViewById(R.id.registerFreelancerBioField);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        if (Account.isLoggedIn && Account.loggedInAccount.getAccountType() == Account.AccountType.CLIENT)
        {
            getMenuInflater().inflate(R.menu.client_logged_in_menu, menu);
            Utilities.menuState = Utilities.MenuState.CLIENT_LOGGED_IN;
        }
        else if (Account.isLoggedIn && Account.loggedInAccount.getAccountType() == Account.AccountType.FREELANCER)
        {
            getMenuInflater().inflate(R.menu.freelancer_logged_in_menu, menu);
            Utilities.menuState = Utilities.MenuState.FREELANCER_LOGGED_IN;
        }
        else
        {
            getMenuInflater().inflate(R.menu.logged_out_menu, menu);
            Utilities.menuState = Utilities.MenuState.LOGGED_OUT;
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        if (Utilities.menuState == Utilities.MenuState.LOGGED_OUT)
        {
            if (id == R.id.menuItemLogin)
            {
                Intent i = new Intent(this, LoginPage.class);
                startActivity(i);
                return true;
            }
            else if (id == R.id.menuItemRegister)
            {
                Intent i = new Intent(this, MainActivity.class);
                startActivity(i);
                return true;
            }
            else if (id == R.id.menuItemFreelancers)
            {
                Intent i = new Intent(this, FreelancerListPage.class);
                startActivity(i);
                return true;
            }
            else if (id == R.id.menuItemQuit)
            {
                finishAffinity();
                return true;
            }
        }

        else if (Utilities.menuState == Utilities.MenuState.CLIENT_LOGGED_IN)
        {
            if (id == R.id.menuItemProfile)
            {
                Intent i = new Intent(this, ClientProfilePage.class);
                startActivity(i);
                return true;
            }
            else if (id == R.id.menuItemFreelancers)
            {
                Intent i = new Intent(this, FreelancerListPage.class);
                startActivity(i);
                return true;
            }
            else if (id == R.id.menuItemLogout)
            {
                Account.isLoggedIn = false;
                Account.loggedInAccount = null;
                startActivity(new Intent(this, MainActivity.class));
                return true;
            }
            else if (id == R.id.menuItemQuit)
            {
                finishAffinity();
                return true;
            }
        }

        else if (Utilities.menuState == Utilities.MenuState.FREELANCER_LOGGED_IN)
        {
            if (id == R.id.menuItemProfile)
            {
                Intent i = new Intent(this, FreelancerProfilePage.class);
                startActivity(i);
                return true;
            }
            else if (id == R.id.menuItemLogout)
            {
                Account.isLoggedIn = false;
                Account.loggedInAccount = null;
                startActivity(new Intent(this, MainActivity.class));
                return true;
            }
            else if (id == R.id.menuItemQuit)
            {
                finishAffinity();
                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    public void onClickRegister(View v)
    {
        boolean exists = false;
        String emailText = email.getText().toString();
        String passwordText = password.getText().toString();
        String confirmPasswordText = confirmPassword.getText().toString();
        String phoneText = phone.getText().toString();
        String websiteText = website.getText().toString();
        String nameText = name.getText().toString();
        String locationText = location.getText().toString();
        String titleText = title.getText().toString();
        String bioText = bio.getText().toString();

        for (Account a : Utilities.accounts)
        {
            if (a.getEmail().equals(emailText))
            {
                exists = true;
                break;
            }
        }

        if (emailText.isEmpty() || passwordText.isEmpty() || confirmPasswordText.isEmpty() || nameText.isEmpty() || titleText.isEmpty())
        {
            Utilities.ShowToast(this, getString(R.string.registerFailedEmpty), true);
        }
        else if (exists)
        {
            Utilities.ShowToast(this, getString(R.string.registerFailedExists), true);
        }
        else if (!passwordText.equals(confirmPasswordText))
        {
            Utilities.ShowToast(this, getString(R.string.registerFailedPassword), true);
        }
        else
        {
            FreelancerAccount account = new FreelancerAccount(emailText, passwordText);
            account.setPhoneNumber(phoneText);
            account.setWebsite(websiteText);
            account.setName(nameText);
            account.setLocation(locationText);
            account.setTitle(titleText);
            account.setBio(bioText);

            Utilities.accounts.add(account);

            Account.isLoggedIn = true;
            Account.loggedInAccount = account;

            Intent i = new Intent(this, FreelancerProfilePage.class);
            i.putExtra(Utilities.EMAIL, emailText);
            startActivity(i);
        }
    }

    public void onClickLogin(View v)
    {
        Intent i = new Intent(this, LoginPage.class);
        startActivity(i);
    }

    public void onClickClient(View v)
    {
        Intent i = new Intent(this, RegisterClientPage.class);
        startActivity(i);
    }
}
