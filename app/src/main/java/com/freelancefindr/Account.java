package com.freelancefindr;

import android.graphics.drawable.Drawable;

public class Account
{


    // For keeping track of the logged in user, if logged in
    //===========================================
    public static boolean isLoggedIn = false;
    public static Account loggedInAccount = null;
    //===========================================

    public enum AccountType
    {
        FREELANCER,
        CLIENT
    }

    private AccountType type;

    // Passwords will obviously not be stored in plaintext for the final product,
    // But it makes developing and debugging much easier.
    // They'll also be in a database.
    // As will all accounts and their info.
    private String email, password, location, name, phoneNumber, website;
    private Drawable image;

    protected Account(String e, String p, AccountType t)
    {
        email = e;
        password = p;
        type = t;
    }

    public boolean checkCredentials(String e, String p)
    {
        return e.equals(email) && p.equals(password);
    }

    public void setEmail(String e)
    {
        email = e;
    }

    public void setPassword(String p)
    {
        password = p;
    }

    public void setLocation(String l)
    {
        location = l;
    }

    public void setName(String n)
    {
        name = n;
    }

    public void setPhoneNumber(String p)
    {
        phoneNumber = p;
    }

    public void setWebsite(String w)
    {
        website = w;
    }

    public void setImage(Drawable d)
    {
        image = d;
    }

    public String getEmail()
    {
        return email;
    }

    public AccountType getAccountType()
    {
        return type;
    }

    public String getLocation()
    {
        return location;
    }

    public String getName()
    {
        return name;
    }

    public String getPhoneNumber()
    {
        return phoneNumber;
    }

    public String getWebsite()
    {
        return website;
    }

    public Drawable getImage()
    {
        return image;
    }

}
