package com.freelancefindr;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

public class FreelancerProfilePage extends AppCompatActivity {

    private FreelancerAccount account;

    private String email, phone, website, name, location, jobTitle, bio;
    private TextView emailView, phoneView, websiteView, nameView, locationView, jobTitleView, bioView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_freelancer_profile_page);

        String e = getIntent().getStringExtra(Utilities.EMAIL) == null ? Account.loggedInAccount.getEmail() : getIntent().getStringExtra(Utilities.EMAIL);

        for (Account a : Utilities.accounts) {
            if (a.getEmail().equals(e)) {
                try {
                    account = (FreelancerAccount) a;
                    break;
                }
                catch(Exception ex) {
                    finishAffinity();
                }
            }
        }

        email = account.getEmail();
        phone = account.getPhoneNumber();
        website = account.getWebsite();
        name = account.getName();
        location = account.getLocation();
        jobTitle = account.getTitle();
        bio = account.getBio();

        emailView = (TextView) findViewById(R.id.freelancerProfileEmailText);
        phoneView = (TextView) findViewById(R.id.freelancerProfilePhoneText);
        websiteView = (TextView) findViewById(R.id.freelancerProfileWebsiteText);
        nameView = (TextView) findViewById(R.id.freelancerProfileName);
        locationView = (TextView) findViewById(R.id.freelancerProfileLocationText);
        jobTitleView = (TextView) findViewById(R.id.freelancerProfileTitle);
        bioView = (TextView) findViewById(R.id.freelancerProfileBioText);

        nameView.setText(name);
        locationView.setText(location);
        jobTitleView.setText(jobTitle);
        bioView.setText(bio);

        if (!Account.isLoggedIn)
        {
            phoneView.setText(getString(R.string.notLoggedIn));
            websiteView.setText(getString(R.string.notLoggedIn));
            emailView.setText(getString(R.string.notLoggedIn));
            phoneView.setTypeface(null, Typeface.ITALIC);
            websiteView.setTypeface(null, Typeface.ITALIC);
            emailView.setTypeface(null, Typeface.ITALIC);
        }
        else {
            emailView.setText(email);
            phoneView.setText(phone);
            websiteView.setText(website);

            Utilities.removeUnderlines((Spannable)phoneView.getText());
            Utilities.removeUnderlines((Spannable)websiteView.getText());
            Utilities.removeUnderlines((Spannable)emailView.getText());
        }

        Button editProfileBtn = (Button)findViewById(R.id.freelanceProfileEditBtn);
        if (Account.loggedInAccount == null || (Account.loggedInAccount != null && !email.equals(Account.loggedInAccount.getEmail()))) {
            editProfileBtn.setVisibility(View.GONE);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        if (Account.isLoggedIn && Account.loggedInAccount.getAccountType() == Account.AccountType.CLIENT)
        {
            getMenuInflater().inflate(R.menu.client_logged_in_menu, menu);
            Utilities.menuState = Utilities.MenuState.CLIENT_LOGGED_IN;
        }
        else if (Account.isLoggedIn && Account.loggedInAccount.getAccountType() == Account.AccountType.FREELANCER)
        {
            getMenuInflater().inflate(R.menu.freelancer_logged_in_menu, menu);
            Utilities.menuState = Utilities.MenuState.FREELANCER_LOGGED_IN;
        }
        else
        {
            getMenuInflater().inflate(R.menu.logged_out_menu, menu);
            Utilities.menuState = Utilities.MenuState.LOGGED_OUT;
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        if (Utilities.menuState == Utilities.MenuState.LOGGED_OUT)
        {
            if (id == R.id.menuItemLogin)
            {
                Intent i = new Intent(this, LoginPage.class);
                startActivity(i);
                return true;
            }
            else if (id == R.id.menuItemRegister)
            {
                Intent i = new Intent(this, MainActivity.class);
                startActivity(i);
                return true;
            }
            else if (id == R.id.menuItemFreelancers)
            {
                Intent i = new Intent(this, FreelancerListPage.class);
                startActivity(i);
                return true;
            }
            else if (id == R.id.menuItemQuit)
            {
                finishAffinity();
                return true;
            }
        }

        else if (Utilities.menuState == Utilities.MenuState.CLIENT_LOGGED_IN)
        {
            if (id == R.id.menuItemProfile)
            {
                Intent i = new Intent(this, ClientProfilePage.class);
                startActivity(i);
                return true;
            }
            else if (id == R.id.menuItemFreelancers)
            {
                Intent i = new Intent(this, FreelancerListPage.class);
                startActivity(i);
                return true;
            }
            else if (id == R.id.menuItemLogout)
            {
                Account.isLoggedIn = false;
                Account.loggedInAccount = null;
                startActivity(new Intent(this, MainActivity.class));
                return true;
            }
            else if (id == R.id.menuItemQuit)
            {
                finishAffinity();
                return true;
            }
        }

        else if (Utilities.menuState == Utilities.MenuState.FREELANCER_LOGGED_IN)
        {
            if (id == R.id.menuItemProfile)
            {
                Intent i = new Intent(this, FreelancerProfilePage.class);
                startActivity(i);
                return true;
            }
            else if (id == R.id.menuItemLogout)
            {
                Account.isLoggedIn = false;
                Account.loggedInAccount = null;
                startActivity(new Intent(this, MainActivity.class));
                return true;
            }
            else if (id == R.id.menuItemQuit)
            {
                finishAffinity();
                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    public void onClickEditProfile(View v)
    {
        startActivity(new Intent(this, EditFreelancerProfilePage.class));
    }
}
