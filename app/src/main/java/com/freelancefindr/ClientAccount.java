package com.freelancefindr;

public class ClientAccount extends Account {

    private String company;

    public ClientAccount(String e, String p) {
        super(e, p, AccountType.CLIENT);
    }

    public void setCompany(String c) {
        company = c;
    }

    public String getCompany() {
        return company;
    }

}
