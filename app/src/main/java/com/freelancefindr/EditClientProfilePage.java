package com.freelancefindr;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

public class EditClientProfilePage extends AppCompatActivity
{

    private EditText emailView, phoneView, websiteView, nameView, locationView, companyView, passwordView, confirmPasswordView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_client_profile_page);

        emailView = (EditText) findViewById(R.id.clientEditProfileEditEmail);
        passwordView = (EditText) findViewById(R.id.clientEditProfileEditPassword);
        confirmPasswordView = (EditText) findViewById(R.id.clientEditProfileEditConfirmPassword);
        phoneView = (EditText) findViewById(R.id.clientEditProfileEditPhone);
        websiteView = (EditText) findViewById(R.id.clientEditProfileEditWebsite);
        nameView = (EditText) findViewById(R.id.clientEditProfileEditName);
        locationView = (EditText) findViewById(R.id.clientEditProfileEditLocation);
        companyView = (EditText) findViewById(R.id.clientEditProfileEditCompany);

        ClientAccount account = (ClientAccount) Account.loggedInAccount;
        emailView.setText(account.getEmail());
        phoneView.setText(account.getPhoneNumber());
        websiteView.setText(account.getWebsite());
        nameView.setText(account.getName());
        locationView.setText(account.getLocation());
        companyView.setText(account.getCompany());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        if (Account.isLoggedIn && Account.loggedInAccount.getAccountType() == Account.AccountType.CLIENT)
        {
            getMenuInflater().inflate(R.menu.client_logged_in_menu, menu);
            Utilities.menuState = Utilities.MenuState.CLIENT_LOGGED_IN;
        }
        else if (Account.isLoggedIn && Account.loggedInAccount.getAccountType() == Account.AccountType.FREELANCER)
        {
            getMenuInflater().inflate(R.menu.freelancer_logged_in_menu, menu);
            Utilities.menuState = Utilities.MenuState.FREELANCER_LOGGED_IN;
        }
        else
        {
            getMenuInflater().inflate(R.menu.logged_out_menu, menu);
            Utilities.menuState = Utilities.MenuState.LOGGED_OUT;
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        if (Utilities.menuState == Utilities.MenuState.LOGGED_OUT)
        {
            if (id == R.id.menuItemLogin)
            {
                Intent i = new Intent(this, LoginPage.class);
                startActivity(i);
                return true;
            }
            else if (id == R.id.menuItemRegister)
            {
                Intent i = new Intent(this, MainActivity.class);
                startActivity(i);
                return true;
            }
            else if (id == R.id.menuItemFreelancers)
            {
                Intent i = new Intent(this, FreelancerListPage.class);
                startActivity(i);
                return true;
            }
            else if (id == R.id.menuItemQuit)
            {
                finishAffinity();
                return true;
            }
        }

        else if (Utilities.menuState == Utilities.MenuState.CLIENT_LOGGED_IN)
        {
            if (id == R.id.menuItemProfile)
            {
                Intent i = new Intent(this, ClientProfilePage.class);
                startActivity(i);
                return true;
            }
            else if (id == R.id.menuItemFreelancers)
            {
                Intent i = new Intent(this, FreelancerListPage.class);
                startActivity(i);
                return true;
            }
            else if (id == R.id.menuItemLogout)
            {
                Account.isLoggedIn = false;
                Account.loggedInAccount = null;
                startActivity(new Intent(this, MainActivity.class));
                return true;
            }
            else if (id == R.id.menuItemQuit)
            {
                finishAffinity();
                return true;
            }
        }

        else if (Utilities.menuState == Utilities.MenuState.FREELANCER_LOGGED_IN)
        {
            if (id == R.id.menuItemProfile)
            {
                Intent i = new Intent(this, FreelancerProfilePage.class);
                startActivity(i);
                return true;
            }
            else if (id == R.id.menuItemLogout)
            {
                Account.isLoggedIn = false;
                Account.loggedInAccount = null;
                startActivity(new Intent(this, MainActivity.class));
                return true;
            }
            else if (id == R.id.menuItemQuit)
            {
                finishAffinity();
                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    public void onClickSave(View v)
    {
        if (emailView.getText().toString().isEmpty() || nameView.getText().toString().isEmpty() ||
                (!passwordView.getText().toString().isEmpty() && confirmPasswordView.getText().toString().isEmpty()) ||
                (passwordView.getText().toString().isEmpty() && !confirmPasswordView.getText().toString().isEmpty()))
        {
            Utilities.ShowToast(this, getString(R.string.editProfileFailedEmpty), true);
        }
        else if (!passwordView.getText().toString().equals(confirmPasswordView.getText().toString()))
        {
            Utilities.ShowToast(this, getString(R.string.editProfileFailedPassword), true);
        }
        else
        {
            for (Account a : Utilities.accounts)
            {
                if (a.getEmail().equals(Account.loggedInAccount.getEmail()))
                {
                    a.setEmail(emailView.getText().toString());
                    a.setPhoneNumber(phoneView.getText().toString());
                    a.setWebsite(websiteView.getText().toString());
                    a.setName(nameView.getText().toString());
                    a.setLocation(locationView.getText().toString());
                    ((ClientAccount) a).setCompany(companyView.getText().toString());

                    if (!passwordView.getText().toString().isEmpty())
                        a.setPassword(passwordView.getText().toString());

                    Account.loggedInAccount = a;

                    break;
                }
            }
            startActivity(new Intent(this, ClientProfilePage.class));
        }
    }
}
